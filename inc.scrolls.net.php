<?php

/**
 * ScrollsNet
 * 
 * @package Scrolls Bot
 * @author ionstorm66
 * @copyright 2014
 * @version 0.0.3
 * @access public
 */
class ScrollsNet
{
	private $main_socket;
	private $main_host;
	private $lobby_socket;
	private $lobby_host;
	private $lobby_room;
	private $auth_token;
	private $auth_login;
	/**
	 * ScrollsNet::__construct()
	 * 
	 * @param array $config
	 * @return
	 */
	function __construct($config,$time_start)
	{
		$this->lobby_room = 'gamecack';
		$this->main_host = $config['main_host'];
		$this->auth_login = $config['auth'];
		$this->connect($time_start);
	}
	/**
	 * ScrollsNet::open_socket()
	 * 
	 * @param resource $socket
	 * @param string $host
	 * @return boolean
	 */
	private function open_socket(&$socket, $host)
	{
		if (!$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP))
		{
			die("Unable to create socket");
		}

		if (!socket_set_nonblock($socket))
		{
			die("Unable to set nonblock on socket");
		}

		$time = time();
		while (!@socket_connect($socket, $host['ip'], $host['port']))
		{
			$err = socket_last_error($socket);
			if ($err == 115 || $err == 114)
			{
				if ((time() - $time) >= 15)
				{
					socket_close($socket);
					die("Connection timed out");
				}
				sleep(1);
				continue;
			}
			die(socket_strerror($err));
		}
		return true;
	}
	/**
	 * ScrollsNet::send()
	 * 
	 * @param resource $socket
	 * @param string $json
	 * @return boolean
	 */
	public function send(&$socket, $json)
	{
		$length = strlen($json);
		$i = 0;
		while ($i < 10)
		{
			$i++;
			$sent = socket_write($socket, $json, $length);
			if ($sent === false)
			{
				die(socket_strerror(socket_last_error($socket)));
			}
			// Check if the entire message has been sented
			if ($sent < $length)
			{
				// If not sent the entire message.
				// Get the part of the message that has not yet been sented as message
				$json = substr($json, $sent);
				// Get the length of the not sented part
				$length -= $sent;
			} else
			{
				break;
			}
		}
		return true;
	}
	/**
	 * ScrollsNet::recive()
	 * 
	 * @param resource $socket
	 * @return array
	 */
	public function recive(&$socket)
	{
		echo ("Debug - recive |");
		$out = array();
		while (true)
		{
			$data = socket_read($socket, '2500', PHP_NORMAL_READ);
			switch (strlen($data))
			{
				case 0:
					echo ("=");
					break 2;
				case 1:
					echo ("+");
					break;
				default:
					echo ("-");
					$out[] = $data;
					break;
			}
		}
		echo ("|\n");
		return $out;
	}
	/**
	 * ScrollsNet::json_decode()
	 * 
	 * @param array $json
	 * @return array
	 */
	public function json_decode($json)
	{
		foreach ($json as $string)
		{
			if (strlen($string) === 1)
			{
				continue;
			}
			$json_out[] = json_decode($string, true);
			switch (json_last_error())
			{
				case JSON_ERROR_NONE:
					break;
				case JSON_ERROR_DEPTH:
					die("Maximum stack depth exceeded");
					break;
				case JSON_ERROR_STATE_MISMATCH:
					die("Underflow or the modes mismatch");
					break;
				case JSON_ERROR_CTRL_CHAR:
					die("Unexpected control character found");
					break;
				case JSON_ERROR_SYNTAX:
					die("Syntax error, malformed JSON");
					break;
				case JSON_ERROR_UTF8:
					die("Malformed UTF-8 characters, possibly incorrectly encoded");
					break;
				default:
					die("Unknown error");
					break;
			}
		}
		return @$json_out;
	}
	/**
	 * ScrollsNet::main_lobbyaddress()
	 * 
	 * @param resource $socket
	 * @return array
	 */
	private function main_lobbyaddress(&$socket)
	{
		$json = array();
		$json['msg'] = 'LobbyLookup';
		$json = json_encode($json);
		if (!$this->send($socket, $json))
		{
			echo socket_strerror(socket_last_error($socket));
		}
		$json = $this->json_decode($this->recive($socket));
		return $json[1];
	}
	/**
	 * ScrollsNet::auth()
	 * 
	 * @param array $email
	 * @return string
	 */
	private function http_auth($auth)
	{
		$json = array();
		$json['agent']['name'] = "Scrolls";
		$json['agent']['version'] = 1;
		$json['username'] = $auth['email'];
		$json['password'] = $auth['password'];
		$json = json_encode($json);
		$url = 'https://authserver.mojang.com/authenticate';
		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($ch);
		curl_close($ch);

		return json_decode($response, true);
	}
	/**
	 * ScrollsNet::lobby_login()
	 * 
	 * @param resource $socket
	 * @param string $token
	 * @return array
	 */
	private function lobby_login(&$socket, $token)
	{
		$json = array();
		$json['msg'] = 'FirstConnect';
		$json['accessToken'] = $token;
		$json = json_encode($json);
		if (!$this->send($socket, $json))
		{
			echo socket_strerror(socket_last_error($socket));
		}
		$json = $this->recive($socket);
		return $json;
	}
	/**
	 * ScrollsNet::lobby_join_room()
	 * 
	 * @param resource $socket
	 * @param string $room
	 * @return array
	 */
	private function lobby_join_room(&$socket, $room)
	{
		$json = array();
		$json['msg'] = 'RoomEnter';
		$json['roomName'] = $room;
		$json = json_encode($json);
		if (!$this->send($socket, $json))
		{
			echo socket_strerror(socket_last_error($socket));
		}
		$json = $this->recive($socket);
		return $json;
	}
	/**
	 * ScrollsNet::lobby_join_lobby()
	 * 
	 * @param resource $socket
	 * @return array
	 */
	private function lobby_join_lobby(&$socket)
	{
		$json = array();
		$json['msg'] = 'JoinLobby';
		$json = json_encode($json);
		if (!$this->send($socket, $json))
		{
			echo socket_strerror(socket_last_error($socket));
		}
		$json = $this->recive($socket);
		return $json;
	}
	/**
	 * ScrollsNet::ping()
	 * 
	 * @return mixed
	 */
	public function ping()
	{
		$json = array();
		$json['msg'] = 'Ping';
		$json = json_encode($json);
		if (!$this->send($this->lobby_socket, $json))
		{
			echo socket_strerror(socket_last_error($this->lobby_socket));
		}
		$json = $this->recive($this->lobby_socket);
		return $json;
	}
	/**
	 * ScrollsNet::connect()
	 * 
	 * @return boolean
	 */
	private function connect($time_start)
	{
		echo ("Time - " . (microtime(true) - $time_start) . "\n");
		echo ("Debug - connecting to main server \n");
		$this->open_socket($this->main_socket, $this->main_host);
		echo ("Time - " . (microtime(true) - $time_start) . "\n");
		echo ("Debug - fetching login server \n");
		$this->lobby_host = $this->main_lobbyaddress($this->main_socket);
		echo ("Time - " . (microtime(true) - $time_start) . "\n");
		echo ("Debug - connectiong to lobby server \n");
		$this->open_socket($this->lobby_socket, $this->lobby_host);
		echo ("Time - " . (microtime(true) - $time_start) . "\n");
		echo ("Debug - fecthing auth token \n");
		$this->auth_token = $this->http_auth($this->auth_login);
		echo ("Time - " . (microtime(true) - $time_start) . "\n");
		echo ("Debug - logging in \n");
		$blob = $this->lobby_login($this->lobby_socket, $this->auth_token);
		echo ("Time - " . (microtime(true) - $time_start) . "\n");
		echo ("Debug - joining lobby \n");
		$debug = $this->lobby_join_lobby($this->lobby_socket);
		echo ("Time - " . (microtime(true) - $time_start) . "\n");
		echo ("Debug - ping \n");
		$debug = $this->ping();
		echo ("Time - " . (microtime(true) - $time_start) . "\n");
		echo ("Debug - joining room \n");
		$debug = $this->lobby_join_room($this->lobby_socket, $this->lobby_room);
	}
}

?>